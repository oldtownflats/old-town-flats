YOU’LL LOVE COMING HOME - You’ll quickly discover and experience why Fort Collins continues to have the prestigious reputation of being one of the best places to live with a pet-friendly apartment in Old Town Flats. Located in the heart of downtown Fort Collins.

Address: 310 N Mason St, Fort Collins, CO 80524, USA

Phone: 970-484-3454